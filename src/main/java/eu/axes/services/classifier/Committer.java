package eu.axes.services.classifier;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;

import eu.axes.services.classifier.beans.ImageClassifierConfiguration;


/**
 * This class is a delegate for the actual commit steps and for managing the timer.
 * It must be used as a singleton otherwise you might have concurrent commits.
 *
 * @author ymombrun
 * @date 2015-03-27
 */
public class Committer {


	private static final String PATH_SEPARATOR = System.getProperty("path.separator");


	final Log log;


	final ImageClassifierConfiguration configuration;


	final long timeout;


	final List<String> scriptParams;


	final String pythonPath;


	private final Timer timer;


	private CommitTask lastTask;


	private final String collectionId;


	private final long delay;


	/**
	 * @param configuration
	 *            The holder of the configuration shared between Indexer and Analyser.
	 * @param timeout
	 *            The number of ms to be used as timeout when executing run automatic test script.
	 * @param scriptParameters
	 *            The list of limas parameters to be sent to the script
	 * @param collectionId
	 *            The collection id
	 * @param delay
	 *            The delay to wait after last received index request before committing.
	 * @throws IOException
	 *             if an error occurs when validating the configuration
	 */
	public Committer(final ImageClassifierConfiguration configuration, final long timeout, final List<String> scriptParameters, final String collectionId, final long delay) throws IOException {
		this.log = LogFactory.getLog(this.getClass());
		this.configuration = configuration;
		this.configuration.validate();
		this.timeout = timeout;
		this.delay = delay;

		final ClassLoader cl = this.getClass().getClassLoader();
		if (cl instanceof URLClassLoader) {
			final StringBuilder sb = new StringBuilder();
			for (final URL url : ((URLClassLoader) cl).getURLs()) {
				sb.append(url.getPath());
				sb.append(Committer.PATH_SEPARATOR);
			}
			this.pythonPath = sb.toString().substring(0, sb.toString().length() - Committer.PATH_SEPARATOR.length());

			this.log.debug("Python path is: " + this.pythonPath);
		} else {
			final String msg = "Unable to get an URLClassloader instance. It's a " + cl.getClass() + ".";
			this.log.fatal(msg);
			throw new IllegalArgumentException(msg);
		}

		this.scriptParams = scriptParameters;
		this.collectionId = collectionId;

		this.timer = new Timer("ImageClassifierIndexer Timer", true);
		this.log.info("ImageClassifierIndexer successfully initialised.");
	}




	class CommitTask extends TimerTask {


		public CommitTask(final String normalisedCollectionImagePath, final String normalisedJSONCollectionPath) {
			super();
			this.normalisedCollectionImagePath = normalisedCollectionImagePath;
			this.normalisedJSONCollectionPath = normalisedJSONCollectionPath;
		}


		private final String normalisedCollectionImagePath;


		private final String normalisedJSONCollectionPath;


		@Override
		public void run() {
			Committer.this.log.debug("Commit in progress. First generate JSON files and the send entities to LIMAS.");
			final CommandLine run_generate_json = CommandLine.parse(ImageClassifierConfiguration.RUN_GENERATE_JSON_SH + " " + Committer.this.configuration.getMCRFolderAsFile() + " "
					+ this.normalisedCollectionImagePath + " " + this.normalisedJSONCollectionPath + " " + Committer.this.configuration.getModelsFileNormalisedRelativePath());
			try {
				CmdExecutor.executeCommandLine(run_generate_json, Committer.this.configuration.getRunGenerateJsonFolderAsFile(), Committer.this.timeout);
			} catch (final UnexpectedException ue) {
				// Nothing to do, already logged.
				return;
			}


			final CommandLine jythonCmd = CommandLine.parse("java -Dproc_jython -classpath \"" + Committer.this.pythonPath + "\" -Xmx4048m -Dfile.encoding=UTF-8 org.python.util.jython "
					+ StringUtils.join(Committer.this.scriptParams, " "));
			try {
				CmdExecutor.executeCommandLine(jythonCmd, new File("/tmp"), Committer.this.timeout);
			} catch (final UnexpectedException ue) {
				// Nothing to do, already logged.
				return;
			}
			Committer.this.log.debug("Jython Command lines successfully executed.");
		}

	}


	/**
	 * @param force
	 *            Whether or not to force for commit now
	 * @throws InvalidParameterException
	 *             If something wrong append when trying to actually commit.
	 */
	public void askForCommit(final boolean force) throws InvalidParameterException {
		synchronized (this.timer) {
			if (this.lastTask != null) {
				this.lastTask.cancel();
				this.lastTask = null;
			}
			this.timer.purge();

			final File collectionFolder = new File(this.configuration.getCollectionsFolderAsFile(), this.collectionId);

			if ((!collectionFolder.exists() && !collectionFolder.mkdir()) || !collectionFolder.isDirectory()) {
				final String message = "Collection " + this.collectionId + " does not exists, cannot be created or is not a directory. Unable to run the generate JSON.";
				this.log.error(message);
				throw ExceptionFactory.createInvalidParameterException(message);
			}

			final File listFiles = new File(collectionFolder, ImageClassifierConfiguration.LIST_FILES);
			final File listScores = new File(collectionFolder, ImageClassifierConfiguration.LIST_SCORES);

			if (!listFiles.exists() || !listScores.exists()) {
				final String message = "One of " + listFiles.getAbsolutePath() + " or " + listScores.getAbsolutePath() + " does not exists. Unable to run the generate JSON.";
				this.log.error(message);
				throw ExceptionFactory.createInvalidParameterException(message);
			}
			final File jsonCollectionFolder = new File(new File(this.configuration.getRunGenerateJsonFolderAsFile(), ImageClassifierConfiguration.JSON), collectionFolder.getName());
			if (!jsonCollectionFolder.exists() && !jsonCollectionFolder.mkdirs()) {
				final String message = "JSON output folder " + jsonCollectionFolder.getAbsolutePath() + " does not exists. Unable to run the generate JSON.";
				this.log.error(message);
				throw ExceptionFactory.createInvalidParameterException(message);
			}

			final String normalisedCollectionImagePath = FilenameUtils.normalize(collectionFolder.getAbsolutePath()).replace(this.configuration.getRunGenerateJsonFolderAsNormalisedPath() + "/", "");
			final String normalisedJSONCollectionPath = FilenameUtils.normalize(jsonCollectionFolder.getAbsolutePath())
					.replace(this.configuration.getRunGenerateJsonFolderAsNormalisedPath() + "/", "");

			if (force) {
				new CommitTask(normalisedCollectionImagePath, normalisedJSONCollectionPath).run();
			} else {
				this.lastTask = new CommitTask(normalisedCollectionImagePath, normalisedJSONCollectionPath);
				this.timer.schedule(this.lastTask, this.delay);
			}
		}
	}

}
