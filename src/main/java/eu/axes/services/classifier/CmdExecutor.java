package eu.axes.services.classifier;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.services.UnexpectedException;



/**
 * A simple class enabling to execute command lines.
 *
 * @author ymombrun
 */
public class CmdExecutor {


	private static final Log log = LogFactory.getLog(CmdExecutor.class);


	/**
	 * @param cmdLine
	 *            The command line to be executed
	 * @param workingdirectory
	 *            The working directory where to execute the command line
	 * @param timeout
	 *            The timeout to be set to the real executor
	 * @throws UnexpectedException
	 *             If any error occured when executing the script
	 */
	public static void executeCommandLine(final CommandLine cmdLine, final File workingdirectory, final long timeout) throws UnexpectedException {
		final DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		final ExecuteWatchdog watchdog = new ExecuteWatchdog(timeout);

		try (final ByteArrayOutputStream baos = new ByteArrayOutputStream();) {
			try {
				final Executor executor = new DefaultExecutor();
				executor.setExitValue(0);
				executor.setWatchdog(watchdog);
				executor.setWorkingDirectory(workingdirectory);
				executor.setStreamHandler(new PumpStreamHandler(baos));

				CmdExecutor.log.debug("Executing commandline " + cmdLine.toString());

				try {
					executor.execute(cmdLine, resultHandler);
				} catch (final Exception e) {
					final String message = "An error occurs executing command line: " + cmdLine.toString();
					CmdExecutor.log.error(message, e);
					throw ExceptionFactory.createUnexpectedException(message, e);
				}

				try {
					resultHandler.waitFor();
				} catch (final Exception e) {
					final String message = "An error occurs executing command line: " + cmdLine.toString();
					CmdExecutor.log.error(message, e);
					throw ExceptionFactory.createUnexpectedException(message, e);
				}
			} finally {
				try {
					CmdExecutor.log.debug(baos.toString("utf8"));
				} catch (final IOException ioe) {
					CmdExecutor.log.warn("An error occurs writing output of " + cmdLine.getExecutable() + " into the log.", ioe);
				}
			}

			if (resultHandler.getExitValue() != 0) {
				final String message = "The exitValue is not " + 0 + " but " + resultHandler.getExitValue() + " for the command line: '" + cmdLine.toString() + "'.";
				CmdExecutor.log.error(message);
				throw ExceptionFactory.createUnexpectedException(message);
			}
		} catch (final IOException ioe) {
			CmdExecutor.log.warn("An error occured when closing stream.", ioe);
		}

		CmdExecutor.log.debug("Commandline " + cmdLine.toString() + " has been successfully executed.");
	}


}
