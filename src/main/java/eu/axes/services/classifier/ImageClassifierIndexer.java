package eu.axes.services.classifier;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.services.Indexer;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.core.services.indexer.IndexReturn;


/**
 * This class is the indexer service for KUL's ueberclassifier.
 *
 * It is bound to the collection name.
 * It will generate JSON results for the whole collection based on individual results.
 * Then will use this JSON add information inside LIMAS.
 *
 * @author ymombrun
 */
public class ImageClassifierIndexer implements Indexer {


	private final Log log;


	private final Committer committer;



	/**
	 * The constructor
	 *
	 * @param committer
	 *            The committer delegate
	 *
	 */
	public ImageClassifierIndexer(final Committer committer) {
		this.log = LogFactory.getLog(this.getClass());
		this.committer = committer;
		this.log.info("ImageClassifierIndexer successfully initialised.");
	}


	@Override
	public IndexReturn index(final IndexArgs args) throws InvalidParameterException, UnexpectedException {
		this.log.trace("Index method called for ImageClassifierIndexer.");

		if (args == null) {
			final String message = "IndexArgs was null.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}

		this.committer.askForCommit("force".equalsIgnoreCase(args.getUsageContext()));

		return new IndexReturn();
	}

}
