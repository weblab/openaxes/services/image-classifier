package eu.axes.services.classifier;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.purl.dc.elements.DublinCoreAnnotator;

import eu.axes.services.classifier.beans.DetectedClass;
import eu.axes.services.classifier.beans.ImageClassifierConfiguration;
import eu.axes.utils.AxesAnnotator;
import eu.axes.utils.NamingSchemeUtils;



/**
 * This class is the analyser service for KUL's ueberclassifier.
 *
 * It takes as an input an AXES WebLab Document, read the images that are inside and copies them to an internal temporary directory.
 * Then calls a script based on Matlab MCR that are in charge of generating files that describes the classification scores of each image.
 * Finally it parses that output file and enriches the Document with meaning full classes.
 *
 * @author ymombrun
 */
public class ImageClassifierAnalyser implements Analyser {


	/**
	 * The logger to be used inside
	 */
	private final Log log;


	/**
	 * The content manager used to access the keyframes within the WebLab document
	 */
	private final ContentManager contentManager;


	/**
	 * The shared configuration between indexer and analyser
	 */
	private final ImageClassifierConfiguration configuration;


	/**
	 * The timeout to wait during the analysis of the images
	 */
	private final long timeout;


	/**
	 * The uri of the service to be annotated on each annotation created by this service. If null, no produced by statement will be added.
	 */
	private final URI serviceUri;


	/**
	 * The specific configuration of each class to be transfered to the WebLab document
	 */
	private final Collection<DetectedClass> classes;



	/**
	 * The constructor
	 *
	 * @param configuration
	 *            The holder of the configuration shared between Indexer and Analyser.
	 * @param timeout
	 *            The number of ms to be used as timeout when executing run automatic test script.
	 * @param serviceUri
	 *            The service uri used in produced by statements. Might be null.
	 * @param classes
	 *            The collection of classes to be added to the Document
	 * @throws IOException
	 *             if an error occurs when validating the configuration
	 */
	public ImageClassifierAnalyser(final ImageClassifierConfiguration configuration, final long timeout, final String serviceUri, final Collection<DetectedClass> classes) throws IOException {
		this.contentManager = ContentManager.getInstance();
		this.configuration = configuration;
		this.configuration.validate();
		this.log = LogFactory.getLog(this.getClass());
		this.timeout = timeout;
		this.classes = classes;
		if (serviceUri == null) {
			this.serviceUri = null;
		} else {
			this.serviceUri = URI.create(serviceUri);
		}
		this.log.info("ImageClassifierAnalyser successfully initialised.");
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws InvalidParameterException, ContentNotAvailableException, UnexpectedException {
		this.log.trace("Process method called for ImageClassifierAnalyser.");
		final List<Image> images = this.checkProcessArgs(args);
		final Resource resource = args.getResource();

		this.log.debug("Process method called for ImageClassifierAnalyser on resource " + resource.getUri() + " which contains " + images.size() + " image(s).");

		final File videoFolder = this.guessVideoAnalysisFolder(resource);

		if (images.isEmpty()) {
			this.log.warn("Not a single image to classify in resource " + resource.getUri() + ".");
		} else {
			final Map<String, String> imageIdByImageVectorPath = new HashMap<>();
			final Map<String, Image> imageById = new HashMap<>();

			// Read image and feel the maps that are needed when annotating results
			this.copyImagesAndFeelMaps(images, resource, videoFolder, imageIdByImageVectorPath, imageById);

			// Path to the target will that will be generated by the script. If they are already there, just reuse them instead of calling the command line
			final File listFiles = new File(videoFolder, ImageClassifierConfiguration.LIST_FILES);
			final File listScores = new File(videoFolder, ImageClassifierConfiguration.LIST_SCORES);
			final boolean skipGlobalFileAddition = ImageClassifierAnalyser.hasResults(listFiles, listScores);
			if (skipGlobalFileAddition) {
				this.log.info("Results already exist for resource " + resource.getUri() + ". Skipping " + ImageClassifierConfiguration.RUN_AUTOMATIC_TEST_SH + " execution.");
			} else {
				final CommandLine run_automatic_test = CommandLine.parse(ImageClassifierConfiguration.RUN_AUTOMATIC_TEST_SH + " " + this.configuration.getMCRFolderAsFile() + " " + videoFolder);
				CmdExecutor.executeCommandLine(run_automatic_test, this.configuration.getRunAutomaticTestFolderAsFile(), this.timeout);
			}

			if (!ImageClassifierAnalyser.hasResults(listFiles, listScores)) {
				final String message = "No output files have been generated for " + resource.getUri() + ".";
				this.log.error(message);
				throw ExceptionFactory.createUnexpectedException(message);
			}

			// Read results files
			final List<String> listFilesContent = this.readLines(listFiles);
			final List<String> listScoresContent = this.readLines(listScores);
			if ((listFilesContent.isEmpty()) || (listScoresContent.isEmpty())) {
				final String message = "Not a single line in resulting file " + listFiles.getAbsolutePath() + " or " + listScores.getAbsolutePath() + ".";
				this.log.error(message);
				throw ExceptionFactory.createUnexpectedException(message);
			}

			final File collectionFolder = videoFolder.getParentFile();
			final File collectionListFiles = new File(collectionFolder, ImageClassifierConfiguration.LIST_FILES);
			final File collectionListScores = new File(collectionFolder, ImageClassifierConfiguration.LIST_SCORES);
			int err = 0;
			for (int i = 0; i < listFilesContent.size(); i++) {
				final String matFile = listFilesContent.get(i);
				final String imageId = imageIdByImageVectorPath.get(matFile);
				if (imageId == null) {
					this.log.warn(matFile + " appears in " + listFiles.getAbsolutePath() + " but not in the content of current resource; very strange. Maybe we should remove that cache folder.");
					err++;
					continue;
				}
				final String scores = listScoresContent.get(i);
				if (!skipGlobalFileAddition) {
					// When skipGlobalFileAddition is false, results were already there and are thus already part of the global file
					this.writeToGlobalFiles(imageId, scores, collectionListFiles, collectionListScores);
				}

				// Read scores and guess if an annot should be added
				if (!this.classes.isEmpty()) {
					this.annotate(imageById.get(imageId), scores);
				}
			}
			if (err == listFilesContent.size()) {
				final String message = "Every single line of " + listFiles.getAbsolutePath() + " contains images that are not in the map...";
				this.log.warn(message);
				this.log.debug("Map imageIdByImageVectorPath was " + imageIdByImageVectorPath.toString());
				throw ExceptionFactory.createUnexpectedException(message);
			}

			this.log.debug("Resource " + resource.getUri() + " successfully processed.");
		}

		final ProcessReturn pr = new ProcessReturn();
		pr.setResource(args.getResource());
		return pr;
	}


	/**
	 * @param listFiles
	 *            The file that list the .mat files that have been generated
	 * @param listScores
	 *            The file that list the score of each mat file
	 * @return <code>true</code> if both files exists, are files and are not empty.
	 */
	private static boolean hasResults(final File listFiles, final File listScores) {
		return listFiles.exists() && listFiles.isFile() && (listFiles.length() > 0) && listScores.exists() && listScores.isFile() && (listFiles.length() > 0);
	}


	private void copyImagesAndFeelMaps(final List<Image> images, final Resource resource, final File videoFolder, final Map<String, String> imageIdByImageVectorPath, final Map<String, Image> imageById)
			throws UnexpectedException {
		if (!videoFolder.exists() && !videoFolder.mkdirs()) {
			final String message = "An error occured when trying to create the folder " + videoFolder.getAbsolutePath() + " for resource " + resource.getUri() + ".";
			this.log.error(message);
			throw ExceptionFactory.createUnexpectedException(message);
		}

		final ListIterator<Image> imageIterator = images.listIterator();
		while (imageIterator.hasNext()) {
			final Image imageUnit = imageIterator.next();
			final AxesAnnotator imageAA = new AxesAnnotator(imageUnit);
			final File imagePath;
			final String fullImageId, frameId;
			try {
				imagePath = this.contentManager.readNativeContent(imageUnit);
				frameId = NamingSchemeUtils.getFrameId(imageAA, resource);
				fullImageId = NamingSchemeUtils.getAxesImageId(imageAA, imageUnit);
			} catch (final WebLabCheckedException wlce) {
				imageIterator.remove();
				this.log.warn("An required annotation is missing on  + " + imageUnit.getUri() + ".", wlce);
				continue;
			}
			final String extension = '.' + FilenameUtils.getExtension(imagePath.getName());
			final File destFile = new File(videoFolder, frameId + extension);
			if (!destFile.exists() || (destFile.length() != imagePath.length()) || (destFile.lastModified() < imagePath.lastModified())) {
				try {
					FileUtils.copyFile(imagePath, destFile);
				} catch (final IOException ioe) {
					imageIterator.remove();
					this.log.warn("An error occured when copying + " + imagePath + " to " + videoFolder + ".", ioe);
					continue;
				}
			}

			final String imageVectorRelativePath = FilenameUtils.normalize(destFile.getAbsolutePath()).replace(extension, ".mat");
			imageIdByImageVectorPath.put(imageVectorRelativePath, fullImageId);
			imageById.put(fullImageId, imageUnit);
		}

		if (images.isEmpty()) {
			final String message = "An error occured when reading annotation and/or copying every single keyframe of the video " + resource.getUri() + ".";
			this.log.error(message);
			throw ExceptionFactory.createUnexpectedException(message);
		}
	}


	private void annotate(final Image image, final String scores) {
		final String[] scoreArray = StringUtils.split(scores);

		DublinCoreAnnotator dca = null;
		for (final DetectedClass clazz : this.classes) {
			double doubleValue;
			try {
				doubleValue = Double.valueOf(scoreArray[clazz.getArrayLocation()]).doubleValue();
			} catch (final NumberFormatException nfe) {
				this.log.warn("Score for image " + image.getUri() + " at location " + clazz.getArrayLocation() + " is not a valid double. (" + scoreArray[clazz.getArrayLocation()] + ").");
				continue;
			}

			if (doubleValue > clazz.getThreshold()) {
				if (dca == null) {
					final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(image);
					if (this.serviceUri != null) {
						new WProcessingAnnotator(URI.create(annot.getUri()), annot).writeProducedBy(this.serviceUri);
					}
					dca = new DublinCoreAnnotator(URI.create(image.getUri()), annot);
				}
				dca.writeSubject(clazz.getLabel());
			}
		}
	}


	private synchronized void writeToGlobalFiles(final String key, final String score, final File listFiles, final File listScores) throws UnexpectedException {
		try {
			FileUtils.writeLines(listFiles, Collections.singleton(key), true);
			FileUtils.writeLines(listScores, Collections.singleton(score), true);
		} catch (final IOException ioe) {
			final String message = "An error occured when writing scores and/or key for " + key + ".";
			this.log.error(message, ioe);
			throw ExceptionFactory.createUnexpectedException(message);
		}
	}


	private List<String> readLines(final File listFiles) throws UnexpectedException {
		final List<String> listFilesContent;
		try {
			listFilesContent = FileUtils.readLines(listFiles);
		} catch (final IOException ioe) {
			final String message = "Cannot read content of file " + listFiles.getAbsolutePath() + " that should have been generated by script " + ImageClassifierConfiguration.RUN_AUTOMATIC_TEST_SH;
			this.log.error(message, ioe);
			throw ExceptionFactory.createUnexpectedException(message, ioe);
		}
		return listFilesContent;
	}



	/**
	 * Using collection Id and video Id annotated on the given resource guess the name of the folder that will be (or has been already) used to store the keyframes and the texts files results of the
	 * UeberClassifier command line.
	 *
	 * @param resource
	 *            The resource to be used to read collection and video id
	 * @return The folder abstract path that will be used to store images and the list images and list score files.
	 * @throws InvalidParameterException
	 *             If the resource does not contain video or collection id which are mandatory
	 */
	private File guessVideoAnalysisFolder(final Resource resource) throws InvalidParameterException {
		final AxesAnnotator aa = new AxesAnnotator(resource);
		final String collectionId;
		final String videoId;
		try {
			collectionId = NamingSchemeUtils.getCollectionId(aa, resource);
			videoId = NamingSchemeUtils.getVideoId(aa, resource);
		} catch (final WebLabCheckedException wlce) {
			final String message = "A required annotation is missing on resource " + resource.getUri() + ".";
			this.log.error(message, wlce);
			throw ExceptionFactory.createInvalidParameterException(message, wlce);
		}
		return new File(new File(this.configuration.getCollectionsFolderAsFile(), collectionId), videoId);
	}


	/**
	 * @param args
	 *            The process args received
	 * @return The list of images with the WebLab resource.
	 * @throws InvalidParameterException
	 *             If <code>args</code> is <code>null</code> or resource inside <code>args</code> is <code>null</code>.
	 */
	private List<Image> checkProcessArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = "ProcessArgs was null.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		final Resource res = args.getResource();
		if (res == null) {
			final String message = "Resource in ProcessArgs was null.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		return ResourceUtil.getSelectedSubResources(res, Image.class);
	}

}
