package eu.axes.services.classifier.beans;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * This class is a simple bean that contains configuration fields that are shared between the Analyser and the Indexer interfaces of the KUL UeberClassifier.
 * It also contains the shared static fields.
 *
 * @author ymombrun
 */
public final class ImageClassifierConfiguration {


	private final Log log = LogFactory.getLog(this.getClass());



	/**
	 * The name of the file that list the .mat files ({@value #LIST_FILES}).
	 */
	public static final String LIST_FILES = "listfiles.txt";


	/**
	 * The name of the file that list the scores for each file (by row) and each class (by column) ({@value #LIST_SCORES}).
	 */
	public static final String LIST_SCORES = "listscores.txt";


	/**
	 * The script to be executed ({@value #RUN_AUTOMATIC_TEST_SH}).
	 */
	public static final String RUN_AUTOMATIC_TEST_SH = "./run_automatic_test.sh";


	/**
	 * The script to be executed ({@value #RUN_GENERATE_JSON_SH}).
	 */
	public static final String RUN_GENERATE_JSON_SH = "./run_generate_json.sh";



	/**
	 * The name of the base folder that will contain the JSON files.
	 */
	public static final String JSON = "JSON";


	private File collectionsFolder;


	private File MCRFolder;


	private File runAutomaticTestFolder;


	private File runGenerateJsonFolder;


	private File modelsFile;


	private String modelsFileNormalisedRelativePath;


	private String runAutomaticTestFolderNormalisedPath;


	private String runGenerateJsonFolderNormalisedPath;


	/**
	 * @return The folder that contains every single collections
	 */
	public File getCollectionsFolderAsFile() {
		return this.collectionsFolder;
	}


	/**
	 * @return The normalised path to the automatic test folder
	 */
	public String getRunAutomaticTestFolderAsNormalisedPath() {
		return this.runAutomaticTestFolderNormalisedPath;
	}


	/**
	 * @return The normalised path to the generate json folder
	 */
	public String getRunGenerateJsonFolderAsNormalisedPath() {
		return this.runGenerateJsonFolderNormalisedPath;
	}


	/**
	 * @return The folder that contains the Matlab Compiler Runtime binaries
	 */
	public File getMCRFolderAsFile() {
		return this.MCRFolder;
	}


	/**
	 * @return The folder that contains the automatic test script
	 */
	public File getRunAutomaticTestFolderAsFile() {
		return this.runAutomaticTestFolder;
	}


	/**
	 * @return The folder that contains the generate json script
	 */
	public File getRunGenerateJsonFolderAsFile() {
		return this.runGenerateJsonFolder;
	}



	/**
	 * @param collectionsFolder
	 *            The folder that contains every single collections
	 */
	public void setCollectionsFolder(final String collectionsFolder) {
		this.collectionsFolder = new File(collectionsFolder);
	}


	/**
	 * @param MCRFolder
	 *            The folder that contains the Matlab Compiler Runtime binaries
	 */
	public void setMCRFolder(final String MCRFolder) {
		this.MCRFolder = new File(MCRFolder);
	}


	/**
	 * @param runAutomaticTestFolder
	 *            The folder that contains the automatic test script
	 */
	public void setRunAutomaticTestFolder(final String runAutomaticTestFolder) {
		this.runAutomaticTestFolder = new File(runAutomaticTestFolder);
	}


	/**
	 * @param runGenerateJsonFolder
	 *            The folder that contains the generate json script
	 */
	public void setRunGenerateJsonFolder(final String runGenerateJsonFolder) {
		this.runGenerateJsonFolder = new File(runGenerateJsonFolder);
	}


	/**
	 * @param modelsFile
	 *            the modelsFile to set
	 */
	public void setModelsFile(final String modelsFile) {
		this.modelsFile = new File(modelsFile);
	}


	/**
	 * @return the modelsFileNormalisedRelativePath
	 */
	public String getModelsFileNormalisedRelativePath() {
		return this.modelsFileNormalisedRelativePath;
	}



	/**
	 * Checks that the configuration is valid. I.e. required attributes are set to valid values (existing folders for instance).
	 *
	 * @throws IOException
	 *             If this is not valid or if an error occurs when checking its validity.
	 */
	public void validate() throws IOException {
		if (!this.validateFile("collectionsFolder", this.collectionsFolder, true)) {
			this.collectionsFolder = this.collectionsFolder.getAbsoluteFile();
		}
		if (!this.validateFile("MCRFolder", this.MCRFolder, true)) {
			this.MCRFolder = this.MCRFolder.getAbsoluteFile();
		}
		if (!this.validateFile("runAutomaticTestFolder", this.runAutomaticTestFolder, true)) {
			this.runAutomaticTestFolder = this.runAutomaticTestFolder.getAbsoluteFile();
		}
		if (!this.validateFile("runGenerateJsonFolder", this.runGenerateJsonFolder, true)) {
			this.runGenerateJsonFolder = this.runGenerateJsonFolder.getAbsoluteFile();
		}
		if (!FileUtils.directoryContains(this.runAutomaticTestFolder, this.collectionsFolder)) {
			final String message = this.collectionsFolder + " is not a child of " + this.runAutomaticTestFolder + " but should.";
			this.log.fatal(message);
			throw new IOException(message);
		}
		if (!FileUtils.directoryContains(this.runGenerateJsonFolder, this.runAutomaticTestFolder)) {
			final String message = this.runAutomaticTestFolder + " is not a child of " + this.runGenerateJsonFolder + " but should.";
			this.log.fatal(message);
			throw new IOException(message);
		}
		if (!this.validateFile("modelsFile", this.modelsFile, false)) {
			this.modelsFile = this.modelsFile.getAbsoluteFile();
		}

		this.runAutomaticTestFolderNormalisedPath = FilenameUtils.normalize(this.runAutomaticTestFolder.getAbsolutePath());
		this.runGenerateJsonFolderNormalisedPath = FilenameUtils.normalize(this.runGenerateJsonFolder.getAbsolutePath());
		this.modelsFileNormalisedRelativePath = FilenameUtils.normalize(this.modelsFile.getAbsolutePath()).replace(this.runGenerateJsonFolderNormalisedPath + "/", "");
	}


	private boolean validateFile(final String fileName, final File filePath, final boolean isDirectory) throws IOException {
		if (filePath == null) {
			final String message = fileName + " is null.";
			this.log.fatal(message);
			throw new IOException(message);
		}
		if (!filePath.exists()) {
			final String message = fileName + " does not exist.";
			this.log.fatal(message);
			throw new FileNotFoundException(message);
		}
		if (isDirectory && !filePath.isDirectory()) {
			final String message = fileName + " is not a directory.";
			this.log.fatal(message);
			throw new IOException(message);
		} else if (!isDirectory && !filePath.isFile()) {
			final String message = fileName + " is not a file.";
			this.log.fatal(message);
			throw new IOException(message);
		} else if (!isDirectory && !filePath.canRead()) {
			final String message = fileName + " is not a readable file.";
			this.log.fatal(message);
			throw new IOException(message);
		}
		return filePath.isAbsolute();
	}

}
