package eu.axes.services.classifier.beans;




/**
 * Just a bean that holds the configuration of a detected class
 *
 * @author ymombrun
 */
public class DetectedClass {


	private static final double DEFAULT_THRESHOLD = 0.5d;


	private int arrayLocation;


	private String label;


	private double threshold = DetectedClass.DEFAULT_THRESHOLD;


	/**
	 * @return The location in the array of score
	 */
	public int getArrayLocation() {
		return this.arrayLocation;
	}


	/**
	 * @return the label of the class
	 */
	public String getLabel() {
		return this.label;
	}


	/**
	 * @return The minimum value before inclusion of the detected class
	 */
	public double getThreshold() {
		return this.threshold;
	}


	/**
	 * @param arrayLocation
	 *            The location in the array of score
	 */
	public void setArrayLocation(final int arrayLocation) {
		this.arrayLocation = arrayLocation;
	}


	/**
	 * @param label
	 *            the label of the class
	 */
	public void setLabel(final String label) {
		this.label = label;
	}


	/**
	 * @param threshold
	 *            The minimum value before inclusion of the detected class
	 */
	public void setThreshold(final double threshold) {
		this.threshold = threshold;
	}

}
