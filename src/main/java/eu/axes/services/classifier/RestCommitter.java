package eu.axes.services.classifier;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.services.InvalidParameterException;

/**
 * This class is in charge of calling the actual Committer with force option activated.
 *
 * @author ymombrun
 * @date 2015-03-27
 */
public class RestCommitter {


	private final Committer committer;


	private final Log log;


	/**
	 * The constructor
	 *
	 * @param committer
	 *            The shared instance of the actual committer
	 */
	public RestCommitter(final Committer committer) {
		this.log = LogFactory.getLog(this.getClass());
		this.committer = committer;
	}


	/**
	 * @return A simple text message
	 * @throws InvalidParameterException
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String commit() throws InvalidParameterException {
		this.log.debug("Forced commit called on the UI.");
		this.committer.askForCommit(true);
		this.log.debug("Forced commit successfull.");
		return "Commit successfull\n";
	}

}
